//
//  ExampleAPIDataSource.swift
//  MTXThirdPartyCard
//
//  Created by Ángel González Abad on 18/6/18.
//  Copyright © 2018 Radmas. All rights reserved.
//

import iOSBase

internal class ExampleAPIDataSource_Cpt {
    private var provider: ServerProviderProtocol
    // Do api call operations
    init(provider: ServerProviderProtocol) {
        self.provider = provider
    }
}
