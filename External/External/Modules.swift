//
//  Modules.swift
//  MTXThirdPartyCard
//
//  Created by Jose Marbin Tapia Rodríguez on 11/4/18.
//  Copyright © 2018 Radmas. All rights reserved.
//

import Foundation
import Viperit

// MARK: - Card modules
// swiftlint:disable identifier_name
internal enum ThirdPartyCardModules: String, ViperitModule {
    case Greeting

    var viewType: ViperitViewType {
        switch self {
        default:
            return .nib
        }
    }
}
