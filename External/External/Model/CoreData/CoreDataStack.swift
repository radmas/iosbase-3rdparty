//
//  CoreDataStack.swift
//  iOSBase
//
//  Created by David Merillas on 4/9/17.
//  Copyright © 2017 Radmas. All rights reserved.
//

import CoreData
import UIKit

internal enum CoreDataError: Error {
    case unableToGetContext
    case unableToSaveContext
    case nonMigratableModels
}

internal final class CoreDataStack {
    static let sharedInstance = CoreDataStack()
    private let managedObjModelContainerExt: String = "momd"
    private let managedObjModelExt: String = "mom"
    private var modelName: String = "External"
    private lazy var applicationDocumentsDirectory: URL = {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls.last!
    }()

    private lazy var modelURL: URL = {
        MTXthirdPartyCardSDK_Cpt.sharedInstance.getBundle_Cpt()
            .url(forResource: self.modelName,
                 withExtension: managedObjModelContainerExt)!
    }()

    private lazy var managedObjectModel: NSManagedObjectModel = {
        /* The managed object model for the application. This property is not optional.
         * It is a fatal error for the application not to be able to find and load its model.
         */
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()

    private lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        /* The persistent store coordinator for the application. This implementation creates and returns a coordinator,
         * having added the store for the application to it.
         * This property is optional since there are legitimate error conditions
         * that could cause the creation of the store to fail.
         */
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("\(self.modelName).sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            print("MTXthirdPartyCard: Adding persistent stores...")
            try coordinator
                .addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: [
                    NSInferMappingModelAutomaticallyOption: true,
                    NSMigratePersistentStoresAutomaticallyOption: true,
                    NSPersistentStoreFileProtectionKey: FileProtectionType.complete
                ])
        } catch {
            print("MTXthirdPartyCard: Failure adding store -> \(error.localizedDescription)")
            print("MTXthirdPartyCard: Forcing recreation of database...")
            do {
                try self.forceRemoveDatabase()
                try coordinator
                    .addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: [
                        NSInferMappingModelAutomaticallyOption: true,
                        NSMigratePersistentStoresAutomaticallyOption: true,
                        NSPersistentStoreFileProtectionKey: FileProtectionType.complete
                    ])
            } catch {
                fatalError("MTXthirdPartyCard: All tries failed. Crashing... # Error: \(error.localizedDescription)")
            }
        }
        return coordinator
    }()

    private lazy var managedObjectContext: NSManagedObjectContext? = {
        /* Returns the managed object context for the application
         * (which is already bound to the persistent store coordinator for the application.)
         * This property is optional since there are legitimate error conditions
         * that could cause the creation of the context to fail.
         */
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()

    init() {}

    func getContext() throws -> NSManagedObjectContext {
        if let context = managedObjectContext {
            return context
        } else {
            throw CoreDataError.unableToGetContext
        }
    }

    func saveContext() throws {
        do {
            try managedObjectContext!.save()
        } catch {
            print("Unresolved error \(error)")
            throw CoreDataError.unableToSaveContext
        }
    }

    func purgePersistentData() throws {
        let url = self.applicationDocumentsDirectory.appendingPathComponent("\(self.modelName).sqlite")
        try persistentStoreCoordinator
            .destroyPersistentStore(at: url, ofType: NSSQLiteStoreType, options: [
                NSInferMappingModelAutomaticallyOption: true,
                NSMigratePersistentStoresAutomaticallyOption: true,
                NSPersistentStoreFileProtectionKey: FileProtectionType.complete
            ])
        try persistentStoreCoordinator
            .addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: [
                NSInferMappingModelAutomaticallyOption: true,
                NSMigratePersistentStoresAutomaticallyOption: true,
                NSPersistentStoreFileProtectionKey: FileProtectionType.complete
            ])
    }

    private func forceRemoveDatabase() throws {
        let sqliteFile = self.applicationDocumentsDirectory.appendingPathComponent("\(self.modelName).sqlite")
        let sqliteShm = self.applicationDocumentsDirectory.appendingPathComponent("\(self.modelName).sqlite-shm")
        let sqliteWal = self.applicationDocumentsDirectory.appendingPathComponent("\(self.modelName).sqlite-wal")

        try FileManager.default.removeItem(at: sqliteFile)
        try FileManager.default.removeItem(at: sqliteShm)
        try FileManager.default.removeItem(at: sqliteWal)
    }

    private func migrateStore(at storeURL: URL, momVersions: [NSManagedObjectModel]) throws {
        let idx = try indexOfCompatibleMom(at: storeURL, momVersions: momVersions)
        let restMomVersions = momVersions.suffix(from: (idx + 1))
        guard !restMomVersions.isEmpty else {
            return // migration not necessary
        }
        _ = try restMomVersions.reduce(momVersions[idx]) { smom, dmom in
            try migrateStore(at: storeURL, from: smom, to: dmom)
            return dmom
        }
    }

    private func indexOfCompatibleMom(at storeURL: URL, momVersions: [NSManagedObjectModel]) throws -> Int {
        let meta = try NSPersistentStoreCoordinator
            .metadataForPersistentStore(ofType: NSSQLiteStoreType, at: storeURL)
        guard let idx = momVersions.firstIndex(where: {
            $0.isConfiguration(withName: nil, compatibleWithStoreMetadata: meta)
        }) else {
            throw CoreDataError.nonMigratableModels
        }
        return idx
    }

    private func migrateStore(at storeURL: URL,
                              from smom: NSManagedObjectModel,
                              to dmom: NSManagedObjectModel) throws {
        // Prepare temp directory
        let dir = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(UUID().uuidString)
        try FileManager.default
            .createDirectory(at: dir, withIntermediateDirectories: true, attributes: nil)
        defer {
            _ = try? FileManager.default.removeItem(at: dir)
        }

        // Perform migration
        let mapping = try findMapping(from: smom, to: dmom)
        let destURL = dir.appendingPathComponent(storeURL.lastPathComponent)
        let manager = NSMigrationManager(sourceModel: smom, destinationModel: dmom)
        try autoreleasepool {
            try manager.migrateStore(
                from: storeURL,
                sourceType: NSSQLiteStoreType,
                options: nil,
                with: mapping,
                toDestinationURL: destURL,
                destinationType: NSSQLiteStoreType,
                destinationOptions: nil
            )
        }

        // Replace source store
        let psc = NSPersistentStoreCoordinator(managedObjectModel: dmom)
        try psc.replacePersistentStore(
            at: storeURL,
            destinationOptions: nil,
            withPersistentStoreFrom: destURL,
            sourceOptions: nil,
            ofType: NSSQLiteStoreType
        )
    }

    private func findMapping(from smom: NSManagedObjectModel, to dmom: NSManagedObjectModel) throws -> NSMappingModel {
        if let mapping = NSMappingModel(from: Bundle.allBundles, forSourceModel: smom, destinationModel: dmom) {
            return mapping // found custom mapping
        }
        return try NSMappingModel.inferredMappingModel(forSourceModel: smom, destinationModel: dmom)
    }

    private func getListOfModelVersions() -> [NSManagedObjectModel] {
        var modelVersions: [NSManagedObjectModel] = []
        do {
            let versionURLs: [URL] = try FileManager.default
                .contentsOfDirectory(at: self.modelURL,
                                     includingPropertiesForKeys: nil, options: .skipsHiddenFiles)
            for versionURL in versionURLs {
                if versionURL.pathExtension == managedObjModelExt,
                    let versionModel = NSManagedObjectModel(contentsOf: versionURL) {
                    modelVersions.append(versionModel)
                }
            }
        } catch {
            print("CoreDataStack -> getListOfModelVersions(): \(error)")
        }
        return modelVersions
    }
}
