//
//  ExternalSDK.swift
//  MTXthirdPartyCard
//
//  Created by Jose Marbin Tapia Rodríguez on 11/4/18.
//  Copyright © 2018 Radmas. All rights reserved.
//

import Foundation

// Disabling force_unwrapping rules.
// swiftlint:disable force_unwrapping
public final class MTXthirdPartyCardSDK_Cpt {
    public static let sharedInstance = MTXthirdPartyCardSDK_Cpt()

    public func getBundle_Cpt() -> Bundle {
        return Bundle(for: type(of: self))
    }

    private var exampleSDK: ExampleSDK_Cpt?

    public func getExampleSDK_Cpt() -> ExampleSDK_Cpt {
        if let exampleSDK = self.exampleSDK {
            return exampleSDK
        } else {
            exampleSDK = ExampleSDK_Cpt(repository: getDependencies().getExampleRepository())
        }
        return exampleSDK!
    }

    private var thirdPartyDependencies: ThirdPartyDependencies?
    /** Dependencies Documentation */
    private func getDependencies() -> ThirdPartyDependencies {
        if let thirdPartyDependencies = self.thirdPartyDependencies {
            return thirdPartyDependencies
        } else {
            thirdPartyDependencies = ProductionDependencies()
            return thirdPartyDependencies!
        }
    }
}
