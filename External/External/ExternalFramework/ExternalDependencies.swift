//
//  ExternalDependencies.swift
//  MTXThirdPartyCard
//
//  Created by David Merillas on 22/8/17.
//  Copyright © 2017 Radmas Technologies S.L. All rights reserved.
//

internal protocol ThirdPartyDependencies {
    func getExampleRepository() -> ExampleRepository_Cpt
}

internal final class ProductionDependencies: ThirdPartyDependencies {
    func getExampleRepository() -> ExampleRepository_Cpt {
        return ExampleRepository_Cpt()
    }
}
