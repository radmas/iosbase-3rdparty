//
//  GreetingPresenter.swift
//  MTXThirdPartyCard
//
//  Created by Ángel González Abad on 10/04/2019.
//  Copyright © 2019 Radmas. All rights reserved.
//

import Foundation
import Viperit

class GreetingPresenter: Presenter {
    override func viewHasLoaded() {
        view.prepareView()
    }
}

// swiftlint:disable force_cast
// MARK: - VIPER COMPONENTS API (Auto-generated code)
private extension GreetingPresenter {
    var view: GreetingViewInterface {
        return _view as! GreetingViewInterface
    }
    var interactor: GreetingInteractor {
        return _interactor as! GreetingInteractor
    }
    var router: GreetingRouter {
        return _router as! GreetingRouter
    }
}
