//
//  MTXthirdPartyCardCardTVCell.swift
//  MTXthirdPartyCard
//
//  Created by Ángel González Abad on 5/10/18.
//  Copyright © 2018 Radmas. All rights reserved.
//

import CoreData
import iOSBase
import UIKit
import Viperit

public final class ExternalCardTVCell: BaseCardView_Cpt, AddModuleProtocol {
    override public var className_Cpt: String {
        return "ExternalCardTVCell"
    }

    override public var cellReuseIdentifier_Cpt: String? {
        return "ExternalCardTVCellReuseIdentifier"
    }

    public var cardSettingsAction: [UIAlertAction] = []
    private let notificationName = "ExternalNotification"

    @IBOutlet private weak var defaultImageView: UIImageView!
    @IBOutlet private weak var defaultTextLabel: UILabel!

    override public func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        prepareView()
    }

    override public func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    override public func getCardViewCellHeight_Cpt() -> CGFloat {
        return 260
    }

    override public func getCardViewIcon_Cpt() -> UIImage? {
        return UIImage(named: "cardIcon", in: Bundle(for: type(of: self)), compatibleWith: nil)
    }

    public func addModule_Cpt() {}
    public func getAddCardImage_Cpt() -> UIImage? {
        return UIImage(named: "addthirdPartyCard", in: Bundle(for: type(of: self)), compatibleWith: nil)
    }

    internal func prepareView() {
        var tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.showDetail_Cpt))
        defaultImageView.addGestureRecognizer(tapGesture)
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.showDetail_Cpt))
        defaultTextLabel.addGestureRecognizer(tapGesture)
    }

    @objc
    private func showDetail_Cpt() {
        let module = ThirdPartyCardModules.Greeting.build(bundle: Bundle(for: type(of: self)), deviceType: .phone)
        module.view.viewController.hidesBottomBarWhenPushed = true
        module.router.show(from: (self.navigationController_Cpt?.visibleViewController)!, embedInNavController: false, setupData: nil)
    }
}
