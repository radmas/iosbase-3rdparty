//
//  AppConfigCaller.swift
//  ExternalApp
//
//  Created by Ángel González Abad on 08/04/2019.
//  Copyright © 2019 ExternalEnterprise. All rights reserved.
//

import Foundation
import iOSBase

internal class AppConfigCaller: AppConfigurationDelegate_Cpt {
    private let configDictionary = Bundle.main
        .infoDictionary?["ApplicationConfiguration"] as? [String: AnyObject]

    func getAppVersion_Cpt() -> String? {
        return Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
    }

    func getAppKey_Cpt() -> String? {
        return configDictionary?["app_key"] as? String
    }

    func getBaseURL_Cpt() -> String? {
        return configDictionary?["base_url"] as? String
    }

    func getClientId_Cpt() -> String? {
        return configDictionary?["client_id"] as? String
    }

    func getConfigurationElement_Cpt(named: String) -> String? {
        return configDictionary?[named] as? String
    }

    func getDefaultConfigurationElement_Cpt(named: String) -> String? {
        return configDictionary?["default"]?[named] as? String
    }

    func getLoginType_Cpt() -> String? {
        return configDictionary?["login_type"] as? String
    }

    func getLoginEndpoint_Cpt() -> String? {
        return configDictionary?["login_endpoint"] as? String
    }

    func getStoreData_Cpt() -> [String : String] {
        return configDictionary?["store"] as? [String: String] ?? [:]
    }

    func getAnalyticsURL_Cpt() -> String? {
        return configDictionary?["analytics_url"] as? String
    }
}
