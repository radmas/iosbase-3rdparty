//
//  AppDelegate.swift
//  ExternalApp
//
//  Created by Ángel González Abad on 08/04/2019.
//  Copyright © 2019 ExternalEnterprise. All rights reserved.
//

import CocoaLumberjack
import iOSBase
import KeychainAccess
import UIKit
import UserNotifications

@UIApplicationMain
internal final class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    lazy var appConfigCaller: AppConfigCaller = AppConfigCaller()
    private weak var screen: UIView?

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        UIApplication.shared.setMinimumBackgroundFetchInterval(UIApplication.backgroundFetchIntervalMinimum)
        prepareAppConfigurationDelegate_Cpt()
        window = UIWindow(frame: UIScreen.main.bounds)
        let rootVC = BaseSDK_Cpt.sharedInstance.getBaseRootVC_Cpt()
        rootVC.installRootViewController(window!)
        window?.backgroundColor = UIColor.white
        window?.makeKeyAndVisible()
        prepareLogManager()
        prepareNotifications_Cpt()
        prepareCardViewProvider_Cpt()
        return true
    }

    // TODO: Better do this inside SDK?
    func prepareLogManager() {
        DDLog.add(DDTTYLogger.sharedInstance) // TTY = Xcode console
        DDLog.add(DDOSLogger.sharedInstance) // ASL = Apple System Logs
        let cache = URLCache(memoryCapacity: 20 * 1024 * 1024, diskCapacity: 100 * 1024 * 1024, diskPath: nil)
        URLCache.shared = cache
        let fileLogger = DDFileLogger() // File Logger
        fileLogger.rollingFrequency = TimeInterval(60*60*72) // 72 hours
        fileLogger.maximumFileSize = 1024 * 1024; //1 MB
        fileLogger.logFileManager.maximumNumberOfLogFiles = 1
        DDLog.add(fileLogger)
        DDLogInfo("\n")
        DDLogInfo("////////////////////////////////////////////////")
        DDLogInfo("/////        START      OF   LOGS         //////")
        DDLogInfo("////////////////////////////////////////////////")
        DDLogInfo("\n")
    }

    private func updateNotificationsReceived() {
        BaseSDK_Cpt.sharedInstance.getNotificationSDK_Cpt().getNotificationDelegate_Cpt()?.addSavedNotificationsFromCenter()
        BaseSDK_Cpt.sharedInstance.getNotificationSDK_Cpt().getNotificationDelegate_Cpt()?.updateNotReadBadge()
    }

    private func prepareAppConfigurationDelegate_Cpt() {
        BaseSDK_Cpt.sharedInstance.getAppConfigurationSDK_Cpt().setDelegate(delegate: self.appConfigCaller)
    }

    private func prepareCardViewProvider_Cpt() {
        BaseSDK_Cpt.sharedInstance.getCardsSDK_Cpt().setCardProvider_Cpt(cardViewProvider: CardViewProvider())
    }

    private func prepareNotifications_Cpt() {
        // TODO: Pendiente de mejora. Motivo: se hace lo mismo que con las tarjetas, debería buscarse patrón apropiado.
        let notificationRouters: [NotificationActionRouterProtocol] = []
        let baseNotificationDelegate = BaseSDK_Cpt.sharedInstance.getNotificationSDK_Cpt().getNotificationDelegate_Cpt()
        let currentNotificationCenter = UNUserNotificationCenter.current()
        baseNotificationDelegate?.setNotificationActionRouters(actionRouters: notificationRouters)
        currentNotificationCenter.delegate = baseNotificationDelegate
        currentNotificationCenter.requestAuthorization(options: [.alert, .sound]) { (granted: Bool, _) in
            print("Permission granted: \(granted)")
            guard granted else { return }
            self.getNotificationSettings_Cpt()
            notificationRouters.forEach({ $0.prepareNotificationObservers_Cpt() })
        }
    }

    private func getNotificationSettings_Cpt() {
        let currentNotificationCenter = UNUserNotificationCenter.current()
        currentNotificationCenter.getNotificationSettings { settings in
            print("Notification settings: \(settings)")

            guard settings.authorizationStatus == .authorized else { return }
            DispatchQueue.main.sync {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }

    private func addPrivacyLayer_Cpt() {
        screen = snapshotView_Cpt()
        guard let screen = screen else { return }
        addBlur_Cpt(to: screen)
        addLoginLogo_Cpt(to: screen)
        setToWindow_Cpt(view: screen)
    }

    private func snapshotView_Cpt() -> UIView {
        return UIScreen.main.snapshotView(afterScreenUpdates: false)
    }

    private func addBlur_Cpt(to view: UIView) {
        let blurEffect = UIBlurEffect(style: .regular)
        let blurBackground = UIVisualEffectView(effect: blurEffect)
        blurBackground.frame = view.frame
        view.addSubview(blurBackground)
    }

    private func addLoginLogo_Cpt(to view: UIView) {
        let logoImage = UIImage(named: "loginLogo", in: Bundle.main, compatibleWith: nil)
        let logoImageView = UIImageView(image: logoImage)
        logoImageView.tintColor = UIColor(hexadecimalString: BaseSDK_Cpt.sharedInstance
            .getConfigurationSDK_Cpt().getApplicationColor_Cpt())
        view.addSubview(logoImageView)
        logoImageView.center = view.center
    }

    private func setToWindow_Cpt(view: UIView) {
        window?.addSubview(view)
    }

    private func removePrivacyLayer_Cpt() {
        screen?.removeFromSuperview()
    }

    internal func application(_ application: UIApplication,
                              didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let store = Keychain()
        let tokenStringified = deviceToken.map({ String(format: "%02.2hhx", $0) }).joined()
        store["notification_account_code"] = tokenStringified
    }

    internal func application(_ application: UIApplication,
                              didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Remote notification support is unavailable due to error: \(error.localizedDescription)")
    }

    func application(_ application: UIApplication,
                     didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        updateNotificationsReceived()
        completionHandler(.newData)
    }

    internal func applicationWillResignActive(_ application: UIApplication) {
        /* Sent when the application is about to move from active to inactive state.
         * This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message)
         * or when the user quits the application and it begins the transition to the background state.
         *
         * Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks.
         * Games should use this method to pause the game.
         */
        self.addPrivacyLayer_Cpt()
    }

    internal func applicationDidEnterBackground(_ application: UIApplication) {
        /* Use this method to release shared resources, save user data, invalidate timers,
         * and store enough application state information to restore your application to
         * its current state in case it is terminated later.
         *
         * If your application supports background execution,
         * this method is called instead of applicationWillTerminate: when the user quits.
         */
    }

    internal func applicationWillEnterForeground(_ application: UIApplication) {
        /* Called as part of the transition from the background to the active state;
         * here you can undo many of the changes made on entering the background.
         */
        updateNotificationsReceived()
    }

    internal func applicationDidBecomeActive(_ application: UIApplication) {
        /* Restart any tasks that were paused (or not yet started) while the application was inactive.
         * If the application was previously in the background, optionally refresh the user interface.
         */
        updateNotificationsReceived()
        self.removePrivacyLayer_Cpt()
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadReleases"), object: nil)
        }
    }

    internal func applicationWillTerminate(_ application: UIApplication) {
        /* Called when the application is about to terminate.
         * Save data if appropriate. See also applicationDidEnterBackground:.
         */
        BaseSDK_Cpt.sharedInstance.getUserSDK_Cpt().deleteOptionalUpdateRead_Cpt()
    }
}
