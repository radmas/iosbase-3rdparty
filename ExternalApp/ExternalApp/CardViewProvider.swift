//
//  CardViewProvider.swift
//  iOSBaseSDK
//
//  Created by Jose Marbin Tapia Rodríguez on 16/8/18.
//  Copyright © 2018 Radmas. All rights reserved.
//

import Foundation
import iOSBase
import MTXThirdPartyCard

internal class CardViewProvider: CardViewProviderProtocol_Cpt {
    func getCardViewWith_Cpt(id: String) -> Any? {
        var card: BaseCardView_Cpt?

        switch id {
        case "my_work_card":
            card = ExternalCardTVCell()
        default:
            card = nil
        }
        return card
    }

    func getAddCardViewProtocolWith_Cpt(id: String) -> Any? {
        var card: BaseCardView_Cpt?

        switch id {
        case "my_work_card":
            card = ExternalCardTVCell()
        default:
            card = nil
        }
        return card
    }
}
